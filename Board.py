import math


class Board():
    """
    Board of the game
    Brown = empty brown place, denoted None
    yellow = empty white place, denoted " "
    black = a black piece, denoted b or B
    white = a white piece, denoted w or W
    """

    def __init__(self, whiteschijven=range(31, 51), blackschijven=range(1, 21), whitedammen=[], blackdammen=[]):
        self.typePieces = ['w', 'b', 'W', 'B']

        self.rows = 10
        self.cols = 10
        self.board = [[0 for i in range(self.cols)] for j in range(self.rows)]
        for i, row in enumerate(self.board):

            for j, elt in enumerate(row):
                j: int
                if i % 2:
                    if j % 2:
                        self.board[i][j] = " "
                    else:
                        self.board[i][j] = None
                else:
                    if j % 2:
                        self.board[i][j] = None
                    else:
                        self.board[i][j] = " "

        for schijf in whiteschijven:
            row, col = self.notatieToGridLocation(schijf)
            self.board[row][col] = 'w'
        for schijf in blackschijven:
            row, col = self.notatieToGridLocation(schijf)
            self.board[row][col] = 'b'
        for schijf in whitedammen:
            row, col = self.notatieToGridLocation(schijf)
            self.board[row][col] = 'W'
        for schijf in blackdammen:
            row, col = self.notatieToGridLocation(schijf)
            self.board[row][col] = 'B'

        # Notatie board, so that reverse NotatietoGrid is easy.
        self.notatieBoard = [
            [' ', 1, ' ', 2, ' ', 3, ' ', 4, ' ', 5],
            [6, ' ',   7, ' ', 8, ' ', 9, ' ', 10, ' '],
            [' ', 11, ' ', 12, ' ', 13, ' ', 14, ' ', 15],
            [16, ' ', 17, ' ', 18, ' ', 19, ' ', 20, ' '],
            [' ', 21, ' ', 22, ' ', 23, ' ', 24, ' ', 25],
            [26, ' ', 27, ' ', 28, ' ', 29, ' ', 30, ' '],
            [' ', 31, ' ', 32, ' ', 33, ' ', 34, ' ', 35],
            [36, ' ', 37, ' ', 38, ' ', 39, ' ', 40, ' '],
            [' ', 41, ' ', 42, ' ', 43, ' ', 44, ' ', 45],
            [46, ' ', 47, ' ', 48, ' ', 49, ' ', 50, ' ']
        ]

    def notatieToGridLocation(self, notatie):
        """
        Changes a notation such as 32-28 to locations on the board
        :param notatie: 0< x < 51
        :return: row, col
        """
        if not (0 < notatie < 51):
            return "No location possible"

        front, back = math.floor(notatie / 10), notatie % 10
        # columns
        if 0 < back < 6:
            col = back * 2
        elif back == 0:
            col = 9
        else:
            col = (back - 5) * 2 - 1
        col = col - 1

        # rows
        row = front * 2
        if back == 0:
            row = row - 1
        elif back > 5:
            row = row + 1
        return row, col


    def getLocationProperties(self, notatie):
        """

        :param notatie:
        :return: None, None if there is nothing. Else True/False
        """
        if not (0 < notatie < 51):
            print("Notatie out of bounds", notatie)
            return None, None
        locRow, locCol = self.notatieToGridLocation(notatie)
        if self.board[locRow][locCol] is None:
            return None, None
        color = str(self.board[locRow][locCol]).upper()
        isKing = str(self.board[locRow][locCol]).isupper()
        isWhite = color == 'W'
        return isWhite, isKing

    def listNeighbours(self, notatie):
        locRow, locCol = self.notatieToGridLocation(notatie)
        listNeighbours = []
        for i in [-1, 1]:
            if locRow + i < 0:  # it is the left edge
                continue
            for j in [-1, 1]:
                if locCol + j < 0:  # it is the upper edge
                    continue
                try:
                    listNeighbours.append(self.notatieBoard[locRow + i][locCol + j])
                except IndexError:  # Right and below edge
                    pass
        return sorted(listNeighbours)

    def listLines(self,notatie):
        """
        returns the lines of a notation,
        returns the moves the king can do if it is the only piece on the board
        :param notatie:
        :return:
        """
        ret = []
        locRow, locCol = self.notatieToGridLocation(notatie)
        for i in [-1,1]:
            for j in [-1,1]:
                tmpRow, tmpCol = locRow, locCol
                while not (tmpRow + i < 0 or tmpCol + j < 0):
                    try:
                        ret.append(self.notatieBoard[tmpRow + i][ tmpCol + j])
                    except IndexError:
                        break
                    tmpRow += i
                    tmpCol += j

        return ret
    def listDamMoves(self, notatie):
        """
        List dam moves, not including slaan.
        :param notatie:
        :return:
        """
        ret = []
        locRow, locCol = self.notatieToGridLocation(notatie)
        for i in [-1,1]:
            for j in [-1,1]:
                tmpRow, tmpCol = locRow, locCol
                while not (tmpRow + i < 0 or tmpCol + j < 0) :
                    if (self.board[tmpRow][tmpCol] is not None) and (tmpRow != locRow and tmpCol != locCol) :
                        # Then there is something there.
                        ret.pop() # remove it from the list.
                        break
                    else:
                        try:
                            ret.append(self.notatieBoard[tmpRow + i][ tmpCol + j])
                        except IndexError:
                            break
                    tmpRow += i
                    tmpCol += j
        return ret

    def listPossibleMoves(self, notatie: int):
        """
        List all possible moves a piece at <Location> can do
        :return list [ tuples ]
        """
        locRow, locCol = self.notatieToGridLocation(notatie)
        isWhite, isKing = self.getLocationProperties(notatie)
        if isWhite == None:
            return "There is nothing here!"
        if isKing:
            possibleMoves = self.listDamMoves(notatie)

        else:
            possibleMoves = self.listNeighbours(notatie)
            if isWhite:
                possibleMoves = filter(lambda x: x < notatie, possibleMoves)
            else:
                possibleMoves = filter(lambda x: x > notatie, possibleMoves)
        possibleMoves = list(possibleMoves)
        # remove occupied
        ret = []
        for i, locs in enumerate(possibleMoves):
            isWhite, tmp = self.getLocationProperties(locs)
            if isWhite == None:
                ret.append( (notatie,possibleMoves[i] ) )
        return ret

    def listAllPossibleMoves(self, isWhite: bool):
        """ Lists all possible moves for a color, assuming no kings #TODO"""
        ret = []
        for piece in range(1, 51):
            w, k = self.getLocationProperties(piece)
            if isWhite == w:
                ss = self.listPossibleMoves(piece)
                for s in ss : ## flatten the list
                    ret.append(s)
        return ret

    def remove_piece_grid(self, locH: int, locV: int):
        self.board[locH][locV] = None

    def promoteToDam(self, notatielocation):
        locH, locV = self.notatieToGridLocation(notatielocation)

        self.board[locH][locV] = str(self.board[locH][locV]).upper()

    def remove_piece(self, notatieLocation):
        """
        :param notatieLocation:
        :return: the piece removed
        """
        locRow, locCol = self.notatieToGridLocation(notatieLocation)
        ret = self.board[locRow][locCol]
        self.board[locRow][locCol] = None
        return ret

    def add_piece(self,notatieLocation, type):
        """

        :param notatieLocation:
        :param type:
        :return: none if no error, else the error
        """
        locRow, locCol = self.notatieToGridLocation(notatieLocation)
        if self.board[locRow][locCol] is not None:
            return "There is already something there"
        if type not in self.typePieces:
            return type + " is not a valid type"
        self.board[locRow][locCol] = type

        return None


    def __str__(self):
        s = ""
        for row in self.board:
            s = s + (" ".join(map(self.map_stub, row)))
            s = s + "\n"
        return (s)

    def map_stub(self, s):
        if s == None:
            return "."
        else:
            return str(s)


