import Board
import unittest
import Game


class BoardTests(unittest.TestCase):
    def setUp(self):
        self.b = Board.Board()

    def test_notatieToGrid(self):
        b = self.b.board.copy()
        for i in range(1, 51):
            row, col = self.b.notatieToGridLocation(i)
            b[row][col] = i
        out_test = [
            [' ', 1, ' ', 2, ' ', 3, ' ',     4,  ' ', 5],
            [6, ' ', 7, ' ', 8, ' ', 9, ' ',     10, ' '],
            [' ', 11, ' ', 12, ' ', 13, ' ', 14, ' ', 15],
            [16, ' ', 17, ' ', 18, ' ', 19, ' ', 20, ' '],
            [' ', 21, ' ', 22, ' ', 23, ' ', 24, ' ', 25],
            [26, ' ', 27, ' ', 28, ' ', 29, ' ', 30, ' '],
            [' ', 31, ' ', 32, ' ', 33, ' ', 34, ' ', 35],
            [36, ' ', 37, ' ', 38, ' ', 39, ' ', 40, ' '],
            [' ', 41, ' ', 42, ' ', 43, ' ', 44, ' ', 45],
            [46, ' ', 47, ' ', 48, ' ', 49, ' ', 50, ' ']
        ]

        self.assertEqual(b, out_test)

    def test_listNeighbours(self):
        self.assertEqual(self.b.listNeighbours(27), [21, 22, 31, 32])
        self.assertEqual(self.b.listNeighbours(26), [21, 31])  # left edge
        self.assertEqual(self.b.listNeighbours(1), [6, 7])  # upper
        self.assertEqual(self.b.listNeighbours(50), [44, 45])  # bottom
        self.assertEqual(self.b.listNeighbours(25), [20, 30])  # right




class GameTests(unittest.TestCase):
    def setUp(self) -> None:
        self.g = Game.Game()

    def test_moves(self):
        out_test = [
            [' ', 'b', ' ', 'b', ' ', 'b', ' ', 'b', ' ', 'b'],
            ['b', ' ', 'b', ' ', 'b', ' ', 'b', ' ', 'b', ' '],
            [' ', 'b', ' ', 'b', ' ', 'b', ' ', 'b', ' ', 'b'],
            ['b', ' ', 'b', ' ', None, ' ', 'b', ' ', 'b', ' '],
            [' ', None, ' ', None, ' ', 'b', ' ', None, ' ', None],
            [None, ' ', None, ' ', 'w', ' ', None, ' ', None, ' '],
            [' ', 'w', ' ', 'w', ' ', 'w', ' ', 'w', ' ', 'w'],
            ['w', ' ', None, ' ', 'w', ' ', 'w', ' ', 'w', ' '],
            [' ', 'w', ' ', 'w', ' ', 'w', ' ', 'w', ' ', 'w'],
            ['w', ' ', 'w', ' ', 'w', ' ', 'w', ' ', 'w', ' ']]
        self.g.move(32, 28)
        self.g.move(18, 23)
        self.g.move(37, 32)
        self.assertEqual(self.g.state, out_test)

    def test_singleSlaan(self):
        out_test = [
            [' ', 'b', ' ', 'b', ' ', 'b', ' ', 'b', ' ', 'b'],
            ['b', ' ', 'b', ' ', 'b', ' ', 'b', ' ', 'b', ' '],
            [' ', 'b', ' ', 'b', ' ', 'b', ' ', 'b', ' ', 'b'],
            ['b', ' ', 'w', ' ', None, ' ', None, ' ', 'b', ' '],
            [' ', None, ' ', None, ' ', None, ' ', None, ' ', None],
            ['b', ' ', None, ' ', None, ' ', None, ' ', None, ' '],
            [' ', None, ' ', None, ' ', 'w', ' ', None, ' ', 'w'],
            ['w', ' ', None, ' ', 'w', ' ', 'w', ' ', 'w', ' '],
            [' ', 'w', ' ', 'w', ' ', 'w', ' ', 'w', ' ', 'w'],
            ['w', ' ', 'w', ' ', 'w', ' ', 'w', ' ', 'w', ' ']
        ]
        self.g.move(32, 28)
        self.g.move(18, 23)
        self.g.move(37, 32)
        self.g.move(23, 29)
        self.g.slaan_single(34, 23)
        self.g.move(17, 22)
        self.g.slaan_single(28, 17)
        self.g.slaan_single(19, 28)
        self.g.slaan_single(28, 37)
        self.g.slaan_single(37, 26)
        self.assertEqual(self.g.state, out_test)

    def test_damPromotion(self):
        out_test = [
            [' ', 'W', ' ', 'W', ' ', 'W', ' ', 'W', ' ', 'W'],
            [None, ' ', None, ' ', None, ' ', None, ' ', None, ' '],
            [' ', None, ' ', None, ' ', None, ' ', None, ' ', None],
            [None, ' ', None, ' ', None, ' ', None, ' ', None, ' '],
            [' ', None, ' ', None, ' ', None, ' ', None, ' ', None],
            [None, ' ', None, ' ', None, ' ', None, ' ', None, ' '],
            [' ', None, ' ', None, ' ', None, ' ', None, ' ', None],
            [None, ' ', None, ' ', None, ' ', None, ' ', None, ' '],
            [' ', None, ' ', None, ' ', None, ' ', None, ' ', None],
            ['B', ' ', 'B', ' ', 'B', ' ', 'B', ' ', 'B', ' ']
        ]
        self.g = Game.Game(
            setup=[range(6, 11), range(41, 46), [], []]
        )
        self.g.move(6, 1)
        self.g.move(7, 2)
        self.g.move(8, 3)
        self.g.move(9, 4)
        self.g.move(10, 5)
        self.g.move(41, 46)
        self.g.move(42, 47)
        self.g.move(43, 48)
        self.g.move(44, 49)
        self.g.move(45, 50)
        self.assertEqual(self.g.state, out_test)
    def test_damMovesSingleDam(self):
        self.g = Game.Game(
            setup = [[11,14,41,44],[],[28],[]]
        )
        self.assertEqual(self.g.b.listDamMoves(28), [22, 17, 23, 19, 32, 37, 33, 39])


if __name__ == '__main__':
    unittest.main(verbosity=2)
