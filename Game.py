import Board


class Game:
    """

    """
    def __init__(self, setup=None):
        """
        :param setup: [whiteschijven, blackschijven, whitedammen, blackdammen], defaults to starting position.
        """

        if setup is None:
            self.b = Board.Board()
        else:
            whiteschijven = setup[0]
            blackschijven = setup[1]
            whitedammen = setup[2]
            blackdammen = setup[3]
            self.b = Board.Board(whiteschijven,blackschijven,whitedammen,blackdammen)

        self.state = self.b.board

    # TODO: rules
    # TODO: interaction
    def move(self, start, to):
        """
        Move a unit from Start to To
        :param start:
        :param to:
        :return:
        """
        startLocH, startLocV = Board.Board.notatieToGridLocation(self.state, start)
        endLocH, endLocV = Board.Board.notatieToGridLocation(self.state, to)
        s = self.state
        s[startLocH][startLocV], s[endLocH][endLocV] = s[endLocH][endLocV], s[startLocH][startLocV]
        # promotion to dam
        if s[endLocH][endLocV] == 'w' and endLocH ==0 :
            self.b.promoteToDam(to)
        if s[endLocH][endLocV] == 'b' and endLocH ==9 :
            self.b.promoteToDam(to)

    def slaan_single(self, start, to):
        """
        slaan of a single piece, for multiple, do it in succession.
        :param start:
        :param to:
        :return:
        """
        startLocH, startLocV = Board.Board.notatieToGridLocation(self.state, start)
        endLocH, endLocV = Board.Board.notatieToGridLocation(self.state, to)
        geslagenH = int( endLocH + (startLocH-endLocH)/2 )
        geslagenV = int( endLocV + (startLocV-endLocV)/2 )
        # remove geslagen pieces
        self.b.remove_piece_grid(geslagenH,geslagenV)
        # move the piece itself
        self.move(start,to)

if __name__ == '__main__':
    pass

