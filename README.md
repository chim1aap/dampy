#dampy
This is a international daughts program written in python.
This is _not_ checkers. See https://lidraughts.org/variant/standard how to play. 

## install
`pip install -r requirements.txt`
or use the package manager for your distro to install the dependencies. 

## Usage
run `cli.py` whenever that becomes available.

## Todo:

- Cli version
    - 
- Gui version
    - Setup Stuff after initialisation
- Rules 
- Auto meerslag
- list all possible moves
- King rules
