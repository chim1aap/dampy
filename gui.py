import Game
from tkinter import *
from tkinter import ttk

squareSize = 50
schijfPadding = 5  # Padding that a piece has around a tile.
smallPadding = 10  # smaller disk size thingamajick.
schijfSize = 40


def draw_tk_board(frame, g, column=1, row=0, sticky=(N)):
    canvas = Canvas(frame, width=12 * squareSize, height=12 * squareSize)
    canvas.grid(column=column, row=row, sticky=sticky)
    counter = 1
    token = True
    board = g.b
    for j in range(1, 11):
        token = not token
        for i in range(1, 11):
            color = "black" if token else "white"
            canvas.create_rectangle(i * squareSize, j * squareSize, (i + 1) * squareSize, (j + 1) * squareSize,
                                    fill=color)
            # Pieces
            if token:
                isWhite, isKing = board.getLocationProperties(counter)
                if isWhite is not None:
                    color = "white" if isWhite else "black"
                    antiColor = "white" if not isWhite else "black"
                    kingDiff = 5 if isKing else 0
                    canvas.create_oval(i * squareSize + schijfPadding, j * squareSize + schijfPadding,
                                       (i + 1) * squareSize - schijfPadding, (j + 1) * squareSize - schijfPadding,
                                       fill=color, outline=antiColor)
                    canvas.create_oval(i * squareSize + schijfPadding + smallPadding,
                                       j * squareSize + schijfPadding + smallPadding,
                                       (i + 1) * squareSize - schijfPadding - smallPadding,
                                       (j + 1) * squareSize - schijfPadding - smallPadding,
                                       fill=color, outline=antiColor)
                    canvas.create_oval(i * squareSize + schijfPadding, j * squareSize + schijfPadding - kingDiff,
                                       (i + 1) * squareSize - schijfPadding,
                                       (j + 1) * squareSize - schijfPadding - kingDiff,
                                       fill=color, outline=antiColor)
                    canvas.create_oval(i * squareSize + schijfPadding + smallPadding,
                                       j * squareSize + schijfPadding + smallPadding - kingDiff,
                                       (i + 1) * squareSize - schijfPadding - smallPadding,
                                       (j + 1) * squareSize - schijfPadding - smallPadding - kingDiff,
                                       fill=color, outline=antiColor)
                counter += 1
            token = not token
    return canvas


def updateMove():
    try:
        global g
        start = startLoc.get()
        to = toLoc.get()
        g.move(start, to)
        draw_tk_board(boardframe, g)
    except ValueError:
        pass


def updateRemove():
    try:
        global g
        remove = remove_Loc.get()
        g.b.remove_piece(remove)
        draw_tk_board(boardframe, g)
    except ValueError:
        print("ValueError:", remove_Loc.get())


def updateAdd():
    try:
        global g
        pieceType = add_type_loc.get()
        pieceLoc = add_Loc.get()
        g.b.add_piece(pieceLoc, pieceType)
        draw_tk_board(boardframe, g)
    except:
        print("error")


def print_hierarchy(w, depth=0):
    """
    Describes the entire tk tree from root w.
    :param w:
    :param depth: optional: how deep.
    :return:
    """
    print('  ' * depth + w.winfo_class() + ' w=' + str(w.winfo_width()) + ' h=' + str(w.winfo_height()) + ' x=' + str(
        w.winfo_x()) + ' y=' + str(w.winfo_y()))
    for i in w.winfo_children():
        print_hierarchy(i, depth + 1)


if __name__ == '__main__':
    g = Game.Game(
        setup=[[1], [2], [6], [7]]
    )
    g.move(7, 12)
    root = Tk()
    root.title("Dampy")

    mainframe = ttk.Frame(root, padding="3 3 12 12")
    mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
    boardframe = ttk.Frame(mainframe)
    boardframe.grid(column=1, row=1, sticky = W)
    board = draw_tk_board(boardframe, g)

    inputsframe = ttk.Frame(mainframe)
    inputsframe.grid(column=2, row=1, sticky = N)

    root.columnconfigure(0, weight=1)
    root.rowconfigure(0, weight=1)
    # NOTE: Columns, Rows start from 1. 0 is probably the Center.
    # TODO: Drag n Drop
    # TODO: Clear contents of boxes when moved.
    startLoc = IntVar()
    start_txt = ttk.Label(inputsframe, text="start")
    start_entry = ttk.Entry(inputsframe, width=7, text="start", textvariable=startLoc)

    toLoc = IntVar()
    to_txt = ttk.Label(inputsframe, text="to")
    to_entry = ttk.Entry(inputsframe, width=7, text="to", textvariable=toLoc)

    remove_Loc = IntVar()
    remove_txt = ttk.Label(inputsframe, text="rm")
    remove_entry = ttk.Entry(inputsframe, width=7, text="rm", textvariable=remove_Loc)

    add_Loc = IntVar()
    add_type_loc = StringVar()
    add_txt = ttk.Label(inputsframe, text="add")
    add_txt_type = ttk.Label(inputsframe, text="type:")
    add_entry = ttk.Entry(inputsframe, width=7, text="add", textvariable=add_Loc)
    add_entry_type = ttk.Entry(inputsframe, width=7, text="type:", textvariable=add_type_loc)

    # Buttons:
    move_button = ttk.Button(inputsframe, text="move", command=updateMove)
    remove_button = ttk.Button(inputsframe, text="rm", command=updateRemove)
    add_button = ttk.Button(inputsframe, text="add", command=updateAdd)

    start_txt.grid(column=0, row=0)
    start_entry.grid(column=1, row=0)
    to_txt.grid(column=2, row=0)
    to_entry.grid(column=3, row=0)
    move_button.grid(column=4, row=0)



    remove_txt.grid(column=0, row=1)
    remove_entry.grid(column=1, row=1)
    remove_button.grid(column=2, row=1)

    add_txt.grid(column=0, row=2)
    add_entry.grid(column=1, row=2)
    add_txt_type.grid(column=2, row=2)
    add_entry_type.grid(column=3, row=2)
    add_button.grid(column=4, row=2)
    # Buttons
    root.bind("<Return>", updateMove)

    # main loop
    print_hierarchy(root)
    root.mainloop()
